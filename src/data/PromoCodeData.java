package data;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {

	private File promocodes;
	private DateinVerwalter dv;
	
	public PromoCodeData(){
	this.promocodes = new File("Promocode.txt");
	this.dv = new DateinVerwalter(this.promocodes);
	}
	 
	@Override
	public boolean savePromoCode(String code) {
		return dv.schreibe(code);
	}

	@Override
	public boolean isPromoCode(String code) {
		//return dv.contains(code);
		return false;
	}

}
